add_requires("libsdl", {system = false, configs = {shared = false}})
add_requires("libepoxy", {configs = {shared = false}})

set_defaultmode("debug")

target("app")
    set_kind("binary")
    set_default(true)
    set_warnings("all")
    -- add_rules adds an on_config (or other) step with predefined function
    add_rules("mode.debug")
    add_rules("mode.release")
    add_files("./src/*.cpp")
    add_includedirs("./src")
    add_includedirs("./tinyobjloader-c/")
    add_includedirs("./cglm/include/")
    add_packages("libsdl")
    add_packages("libepoxy")
    add_cxxflags("", { tools = "gcc" })

    add_tests("io", {
       files = "tests/test_io.cpp",
       remove_files = "src/main.cpp",
       languages = "c++11",
       -- targetdir = "costam",
       -- pass_outputs = "oi",
    })

    before_test(function (target)
      print(target:targetdir())
      os.cp("tests", target:targetdir())
    end)

    after_link(function (target)
      print(debug.getinfo(1, "n").name .. ":$(buildir)")
      print("installdir: " .. target:installdir())
      print("targetdir: " .. target:targetdir())
    end)

    after_build(function(target)
      os.cp("assets/*", target:targetdir())
    end)

    on_run(function (target)
      os.cd(target:targetdir())
      os.run("./app")
    end)
target_end()
