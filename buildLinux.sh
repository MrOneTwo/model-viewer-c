SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
PROJECT_DIR=$SCRIPT_DIR

mkdir -p ${PROJECT_DIR}/app

INCLUDE_PATHS="\
  -I${PROJECT_DIR}/src \
  -I${PROJECT_DIR}/assimp/build/include \
  -I${PROJECT_DIR}/assimp/include \
  -I${PROJECT_DIR}/tinyobjloader-c \
  -I${PROJECT_DIR}/cglm/include
  "

# libepoxy
INCLUDE_PATHS+=" -I${PROJECT_DIR}/libepoxy/include"
INCLUDE_PATHS+=" -I${PROJECT_DIR}/libepoxy/build/include"


LIBS_PATHS="-L${PROJECT_DIR}/libepoxy/build/src"
LIBS='-l:libepoxy.a -ldl -lpthread -lGL'

# On Void Linux the pkg-config can't find SDL2 after installation through xbps.
# Manually setting PKG_CONFIG_PATH helps.
#SDL2=$(PKG_CONFIG_PATH=/usr/local/lib/pkgconfig pkg-config --cflags --libs sdl2)
#SDL2=$(pkg-config --cflags --libs sdl2)
SDL2=$(sdl2-config --cflags --libs)

# AssImp
LIBS_ASSIMP_PATHS="-L${PROJECT_DIR}/assimp/build/code -L${PROJECT_DIR}/assimp/build/contrib/irrXML"
LIBS_ASSIMP='-lassimp -lIrrXML'

SOURCES="${PROJECT_DIR}/src/main.cpp \
  ${PROJECT_DIR}/src/storage.cpp \
  ${PROJECT_DIR}/src/io.cpp \
  ${PROJECT_DIR}/src/shaders.cpp \
  ${PROJECT_DIR}/src/objects.cpp \
  ${PROJECT_DIR}/src/render.cpp \
  "

g++ $SOURCES -g -Wall -o ${PROJECT_DIR}/app/app $INCLUDE_PATHS $LIBS_PATHS $LIBS $SDL2
echo "Build saved in: ${PROJECT_DIR}"
