const std = @import("std");


pub fn build(b: *std.build.Builder) void {
  const target = b.standardTargetOptions(.{});
  const mode = b.standardReleaseOptions();

  const exe = b.addExecutable("app", null);

  exe.setTarget(target);
  exe.setBuildMode(mode);

  exe.addIncludeDir("./src");
  exe.addIncludeDir("./tinyobjloader-c");

  // const exe_c_flags = &[_][]const u8{
  //   "-std=c99",
  // };
  const exe_cxx_flags = &[_][]const u8{
    "-std=c++11",
    "-fno-exceptions",
  };

  exe.addCSourceFiles(&.{
    "src/main.cpp",
    "src/storage.cpp",
    "src/io.cpp",
    "src/shaders.cpp",
    "src/objects.cpp",
    "src/render.cpp",
  }, exe_cxx_flags);

  switch (exe.target.toTarget().os.tag) {
    .windows => {
    },
    .linux => {
    },
    else => {
      @panic("Unsupported OS!");
    },
  }

  exe.linkLibC();
  exe.linkLibCpp();
  exe.linkSystemLibrary("sdl2");
  exe.linkSystemLibrary("epoxy");
  exe.install();
}
