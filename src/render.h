#ifndef RENDER_H
#define RENDER_H

#include "base.h"
#include "storage.h"

#include "cglm/cglm.h"

#include <epoxy/gl.h>
#include <epoxy/glx.h>

#define CUSTOM_3D_SPACE_MAX_X   10000.0f
#define CUSTOM_3D_SPACE_MAX_Y   10000.0f
#define CUSTOM_3D_SPACE_MAX_Z   10000.0f
#define CUSTOM_3D_SPACE_MIN_X  -10000.0f
#define CUSTOM_3D_SPACE_MIN_Y  -10000.0f
#define CUSTOM_3D_SPACE_MIN_Z  -10000.0f


namespace render
{
extern vec3 V3_UP;
extern vec3 V3_FRONT;

enum VisibleFlags {
   VISIBLE_RENDER_NORMALS = 0b1 << 0,
   VISIBLE_RENDER_BOUNDING_BOX = 0b1 << 1,
};

typedef struct Visible {
   enum VisibleFlags flags;
   GLuint vao;
   GLuint vbo;
   GLuint ebo;
   GLuint colbo;
   GLuint normbo;
   GLuint shader_prog;
} Visible;

void shader_set_mvp(GLuint shader_prog, mat4 model, mat4 view, mat4 projection);

void shader_set_mat4(GLuint shader_prog, mat4 data, char* const name);
void shader_set_vec3(GLuint shader_prog, vec3 data, char* const name);

/* This function creates and compiles the shader.
 * The idea is that it'll create the necessary intermediate GLuint's
 * for that process but return only the actual shader program GLuint,
 * which gets used again and again in rendering loop.
 */
GLuint shader_create(const GLchar *vs_source, const GLchar *fs_source);

int init_from_flags(Visible* vis, storage::Mesh* mesh, GLuint shader_prog, enum VisibleFlags flags);
int vao_add_vbo_pos(Visible* vis, const void* const vertices, const int vert_count);
int vao_begin_definition(Visible* vis, GLuint shader_prog);
int vao_add_ebo(Visible* vis, struct bufu32 indices);
// int vao_add_colors(Visible* vis, const void* const colors, const int colors_count);
int vao_add_normals(Visible* vis, const void* const normals, const int normals_count);
/* Create buffers for interleaved data of vertex positions and normals.
 */
int vao_add_vbo_pos_normals(Visible* vis, const void* const data, const int el_count);
}

#endif // RENDER_H
