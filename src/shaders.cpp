#include "shaders.h"

#include "io.h"
#include "render.h"

#include <cstdlib>
#include <cstring>

#include <SDL.h>

namespace shaders
{

struct shader_src {
   char name[64];
   char *code;
};

static struct shader_src *shaders;
static char *shaders_source_code;

const char *shader_files[] = {
   "shdr_vertex_vert_colors_normals.glsl",
   "shdr_vertex_basic_uniform_color.glsl",
   "shdr_vertex_vert_colors.glsl",
   "shdr_vertex_uniform_colors_normals.glsl",
   "shdr_fragment_lights.glsl",
   "shdr_fragment_geo.glsl",
};
const int shader_files_count = sizeof(shader_files)/sizeof(shader_files[0]);

static_assert(shader_files_count == 6, "Incorrect shaders count!");

struct Shader shdr_geo = {};
struct Shader shdr_light = {};
struct Shader shdr_gizmo = {};

int
load_shaders(void)
{
   shaders_source_code = (char*)malloc(4096 * 2);
   shaders = (struct shader_src*)malloc(1024 * 2);
   memset(shaders_source_code, 0, 4096 * 2);
   memset(shaders,0, 1024 * 2);

   char *write_cursor = shaders_source_code;

   for (int i = 0; i < shader_files_count; i++) {
      const char *filename = shader_files[i];
      int len = io::load_entire_file(filename, write_cursor, 't');

      if (len == 0) {
         fprintf(stderr, "Couldn't load %s", filename);
         return 0;
      }

      strncpy(shaders[i].name, filename, sizeof(shaders[i].name));
      shaders[i].code = write_cursor;

      // Move the cursor and terminate with 0.
      write_cursor += len;
      *write_cursor = 0;
      write_cursor += 1;
   }

   const char *shdr_src_vs = NULL;
   const char *shdr_src_fs = NULL;

   get_shader_source_by_name("shdr_vertex_uniform_colors_normals.glsl", &shdr_src_vs, 'v');
   get_shader_source_by_name("shdr_fragment_geo.glsl", &shdr_src_fs, 'f');
   int id = render::shader_create(shdr_src_vs, shdr_src_fs);

   shdr_geo.gl_id = render::shader_create(shdr_src_vs, shdr_src_fs);
   strncpy(shdr_geo.label, "shdr_geo", 32);
   shdr_geo.features = (enum ShaderFeatures)(
      SHADER_FEATURE_MVP | SHADER_FEATURE_NORMALS | SHADER_FEATURE_POSITIONS | SHADER_FEATURE_UNIFORM_COLOR
   );

   get_shader_source_by_name("shdr_vertex_basic_uniform_color.glsl", &shdr_src_vs, 'v');
   get_shader_source_by_name("shdr_fragment_lights.glsl", &shdr_src_fs, 'f');

   shdr_light.gl_id = render::shader_create(shdr_src_vs, shdr_src_fs);
   strncpy(shdr_light.label, "shdr_light", 32);
   shdr_light.features = (enum ShaderFeatures)(
      SHADER_FEATURE_MVP | SHADER_FEATURE_POSITIONS | SHADER_FEATURE_UNIFORM_COLOR
   );

   get_shader_source_by_name("shdr_vertex_vert_colors.glsl", &shdr_src_vs, 'v');
   get_shader_source_by_name("shdr_fragment_lights.glsl", &shdr_src_fs, 'f');

   shdr_gizmo.gl_id = render::shader_create(shdr_src_vs, shdr_src_fs);
   strncpy(shdr_gizmo.label, "shdr_flat_colors", 32);
   shdr_gizmo.features = (enum ShaderFeatures)(
      SHADER_FEATURE_MVP | SHADER_FEATURE_POSITIONS | SHADER_FEATURE_VERT_COLOR
   );

   return 0;
}

struct Shader
get_shader_geo()
{
   return shdr_geo;
}

struct Shader
get_shader_light()
{
   return shdr_light;
}

struct Shader
get_shader_by_features(enum ShaderFeatures features)
{
   struct Shader* shader;

   if (features == shdr_geo.features) {
      shader = &shdr_geo;
   }
   else if (features == shdr_light.features) {
      shader = &shdr_light;
   }
   else if (features == shdr_gizmo.features) {
      shader = &shdr_gizmo;
   }
   else {
      SDL_Log("Couldn't find shader with requested features 0x%x", features);
   }

   if (shader) {
      SDL_Log("Returning shader %s", shader->label);
   }

   return *shader;
}

int
get_shader_source_by_name(const char *looking_for, const char **out, char v_or_f)
{
   for (int i = 0; i < shader_files_count; i++) {
      char *name = shaders[i].name;
      if (strncmp(name, looking_for, strlen(name)) == 0) {
         *out = shaders[i].code;
         return 0;
      }
   }

   fprintf(stdout, "Couldn't find %s (%c), falling back to default shader!\n", looking_for, v_or_f);
   // TODO(michalc): should use shd_vs_default but this one is shit atm.
   if (v_or_f == 'v') *out = shd_vs_default;
   else *out = shd_fs_default;

   return 0;
}

const char* shd_vs_default =
  "#version 460\n"
  "in vec3 a_position;\n"
  "in vec3 a_color;\n"
  "out vec3 v_color;\n"
  "layout(location = 0) uniform mat4 model;\n"
  "layout(location = 1) uniform mat4 view;\n"
  "layout(location = 2) uniform mat4 projection;\n"
  "void main()\n"
  "{\n"
  "  gl_Position = projection * view * model * vec4(a_position.xyz, 1.0);\n"
  "  v_color = a_color;\n"
  "}\n";

const char* shd_fs_default =
   "#version 460\n"
   "// TODO(mc): why does this create problems?\n"
   "// precision mediump float;\n"
   "// varying is same as in for fragment shader\n"
   "\n"
   "void main()\n"
   "{\n"
   "  gl_FragColor = vec4(vec3(1.0, 0.0, 0.0), 1.0);\n"
   "}\n";

/*
 * VERTEX SHADERS
 */

const char* shd_vs_transformations =
  "#version 120\n"
  // attribute is same as in for vertex shader
  "attribute vec3 a_position;\n"
  "attribute vec3 a_color;\n"
  "attribute vec3 a_normal;\n"
  // varying is same as out for vertex shader
  "varying vec3 v_color;\n"
  "varying vec3 v_normal;\n"
  // uniforms meaning values same for every vert
  "uniform mat4 model;\n"
  "uniform mat4 view;\n"
  "uniform mat4 projection;\n"
  "void main()\n"
  "{\n"
  "  gl_Position = projection * view * model * vec4(a_position.xyz, 1.0);\n"
  "  v_color = a_color;\n"
  "  v_normal = a_normal;\n"
  "}\n";

const char* shd_vs_light =
  "#version 120\n"
  // attribute is same as in for vertex shader
  "attribute vec3 a_position;\n"
  // varying is same as out for vertex shader
  "varying vec3 v_color;\n"
  // uniforms meaning values same for every vert
  "uniform mat4 model;\n"
  "uniform mat4 view;\n"
  "uniform mat4 projection;\n"
  "uniform vec3 color;\n"
  "void main()\n"
  "{\n"
  "  gl_Position = projection * view * model * vec4(a_position.xyz, 1.0);\n"
  "  v_color = color;\n"
  "}\n";

const char* shd_vs_texture =
  "#version 120\n"
  // attribute is same as in for vertex shader
  "attribute vec4 a_position;\n"
  "attribute vec3 a_color;\n"
  "attribute vec2 a_texCoords;\n"
  // varying is same as out for vertex shader
  "varying vec3 v_color;\n"
  "varying vec2 v_texCoords;\n"
  // uniforms meaning values same for every vert
  "uniform mat4 model;\n"
  "uniform mat4 view;\n"
  "uniform mat4 projection;\n"
  "void main()\n"
  "{\n"
  "  gl_Position = projection * view * model * vec4(a_position.xyz, 1.0);\n"
  "  v_color = a_color;\n"
  "  v_texCoords = a_texCoords;\n"
  "}\n";

const char* shd_vs_planes =
  "#version 120\n"
  // attribute is same as in for vertex shader
  "attribute vec4 a_position;\n"
  "attribute vec3 a_color;\n"
  "attribute vec2 a_texCoords;\n"
  // varying is same as out for vertex shader
  "varying vec3 v_color;\n"
  "varying vec2 v_texCoords;\n"
  // uniforms meaning values same for every vert
  "uniform float planeIndex;\n"
  "uniform mat4 model;\n"
  "uniform mat4 view;\n"
  "uniform mat4 projection;\n"
  "void main()\n"
  "{\n"
  "  gl_Position = projection * view * model * vec4(a_position.xyz, 1.0);\n"
  "  gl_Position += (planeIndex * 0.3);\n"
  "  v_color = a_color;\n"
  "  v_texCoords = a_texCoords;\n"
  "}\n";



/*
 * FRAGMENT SHADERS
 */


const char* shd_fs_texture =
  // TODO(mc): why does this create problems?
  //"precision mediump float;\n"
  // varying is same as in for fragment shader
  "varying vec3 v_color;\n"
  "varying vec2 v_texCoords;\n"
  "uniform sampler2D tex;\n"
  "void main()\n"
  "{\n"
  "  gl_FragColor = vec4(v_color.xyz, 1.0);\n"
  //"  gl_FragColor = texture2D(tex, v_texCoords);\n"
  "}\n";

} // namespace
