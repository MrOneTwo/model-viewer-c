#include "io.h"
#include "storage.h"

#include <SDL.h>

#include <dirent.h>

namespace io {

static enum err_io ERR_IO = NONE;

enum err_io erred(void) {
  return ERR_IO;
}

int
list_dir_contents(const char *const dir_path, char* out)
{
   ERR_IO = NONE;

   DIR *const dir = opendir(dir_path);
   if (dir == NULL) {
      ERR_IO = INVALID_FILE_PATH;
   }

   if (ERR_IO == NONE) {
      struct dirent *entry;

      int written = 0;
      while ((entry = readdir(dir)) != NULL) {
         // written += snprintf(buf + written, 512, "%s\n", entry->d_name);
         // snprintf(stderr, "%s\n", entry->d_name);
         memcpy(out + written, entry->d_name, strlen(entry->d_name));
         written += strlen(entry->d_name);
         memcpy(out + written, "\n", 1);
         written += 1;
      }
      // Replace the last \n with a 0, to terminate the string.
      out[--written] = 0;

      closedir(dir);
   }

   return 0;
}

uint64_t
load_entire_file(const char* filename, void* buf, char type)
{
   if (buf == NULL) return 0;

   char mode[2] = {'r', type};

   SDL_RWops* file = SDL_RWFromFile(filename, mode);
   if (file == NULL)
   {
      ERR_IO = INVALID_FILE_PATH;
      SDL_Log("Failed load_entire_file(): %s", filename);
      return 0;
   }

   uint8_t unitSize = 1;
   uint64_t unitCount = SDL_RWsize(file);
   SDL_RWread(file, buf, unitSize, unitCount);
   SDL_RWclose(file);

   return unitCount * unitSize;
}

void
obj_loader_file_reader(void* ctx, const char* filename, int is_mtl, const char* obj_filename, char** buf, long unsigned int* len)
{
  (void)ctx;

  void* tt = storage::get_transient_tail();
  uint32_t bytesRead = load_entire_file(filename, tt, 'b');

  if (bytesRead == 0) {
    SDL_Log("Failed to load an OBJ file! Error: %d", ERR_IO);
    return;
  }

  *len = bytesRead;
  *buf = (char*)tt;
}

/*
 * Reads the whole file and saves it into the transient memory.
 */
/*
internal void*
load_entire_file_to_memory(char* filename, AssetTable* assetTable, uint32* size)
{
  // TODO(mc): probably should be SDL_AllocRW
  // TODO(mc): should it be (void*)
  void* storagePointer = assetTable->storageMemory->transientTail;
  SDL_RWops* fileHandle = SDL_RWFromFile(filename, "rb");
  if (fileHandle == NULL) {
    SDL_Log("Failed LoadEntireFile(): %s", filename);
  }
  // TODO(mc): check if opened properly
  uint8 unitSize = 1;
  uint64 unitCount = SDL_RWsize(fileHandle);
  *size = unitCount;
  SDL_RWread(fileHandle, storagePointer, unitSize, unitCount);
  SDL_RWclose(fileHandle);
  return storagePointer;
}
*/

} // namespace
