#include "storage.h"

#include "io.h"
#define TINYOBJ_LOADER_C_IMPLEMENTATION
#include "tinyobj_loader_c.h"

#include <stdlib.h>
#include "utils.h"


namespace storage
{
// Shared variables.
Memory mem = {};

// Internal variables.
typedef struct Context {
   AssetType loading_type;
} Context;

Context context = {};
AssetTable table = {};

void
init_storage_memory(void)
{
   mem.isInitialized = true;
   mem.transientMemorySize = Megabytes(8);
   mem.persistentMemorySize = Megabytes(128);
   mem.transientMemory = malloc(mem.transientMemorySize);
   mem.persistentMemory = malloc(mem.persistentMemorySize);
   mem.transient_tail = mem.transientMemory;
   mem.persistent_tail = mem.persistentMemory;
}

void*
get_transient_tail(void)
{
   return mem.transient_tail;
}

void*
get_persistent_tail(void)
{
   return mem.persistent_tail;
}

void*
append_to_persistent(struct Memory* mem, void* data, size_t bytes_count, bool zero_terminate)
{
   uint8_t* cursor = (uint8_t*)mem->persistent_tail;
   memcpy(cursor, data, bytes_count);

   if (zero_terminate) {
      *(cursor + bytes_count) = 0;
      mem->persistent_tail = (void*)((uint8_t*)mem->persistent_tail + 1);
   }

   mem->persistent_tail = (void*)((uint8_t*)mem->persistent_tail + bytes_count);

   return (void*)cursor;
}

void
start_loading(AssetType type)
{
   context.loading_type = type;
}

void
end_loading()
{
   context.loading_type = ASSET_NONE;
}

int8_t
load_asset(const char* path, int flags)
{
   int ret = 0;

   fprintf(stdout, "%s:loading %s\n", __FUNCTION__, path);
   switch (context.loading_type)
   {
   case ASSET_MODEL3D_OBJ:
      {
         tinyobj_attrib_t obj_attr;
         tinyobj_shape_t* shapes;
         size_t num_shapes;
         tinyobj_material_t* mats;
         size_t num_mats;

         if (TINYOBJ_SUCCESS != tinyobj_parse_obj(&obj_attr,
                                                  &shapes,
                                                  &num_shapes,
                                                  &mats,
                                                  &num_mats,
                                                  path,
                                                  io::obj_loader_file_reader,
                                                  NULL,
                                                  0U))
         {
            return -1;
         }

         // Store data in the transient buffer, because later we might want to interleave the
         // data. That transformation can be done when moving from transient to persistent
         // memory.
         // Store the actual struct that describes the mesh.
         Mesh* model = (Mesh*)get_transient_tail();
         memset(model, 0, sizeof(struct Mesh));

         uint8_t* cursor = (uint8_t*)get_transient_tail() + sizeof(struct Mesh);

         fprintf(stderr, "%s:saving into %p\n", __FUNCTION__, cursor);
         fprintf(stderr, "%s:shapes count %zu\n", __FUNCTION__, num_shapes);

         // Save the name + null terminator.
         model->name = (char*)cursor;
         memcpy(cursor, path, strlen(path));
         cursor += strlen(path);
         *cursor = 0;
         cursor += 1;

         if (flags & MESH_FL_VERTICES) {
            // Save vertices.
            memcpy(cursor, obj_attr.vertices, sizeof(float) * 3 * obj_attr.num_vertices);
            model->vertices.data = (float*)cursor;
            model->vertices.len = obj_attr.num_vertices;
            fprintf(stderr, "%s:obj_attr.num_vertices %d\n", __FUNCTION__, obj_attr.num_vertices);
            cursor += (sizeof(float) * 3 * obj_attr.num_vertices);

            model->flags |= MESH_FL_VERTICES;
         }

         if (flags & MESH_FL_INDICES) {
            // Save indices.
            fprintf(stderr, "%s:obj_attr.num_face_num_verts %d\n", __FUNCTION__, obj_attr.num_face_num_verts);
            uint32_t indices_count = 0;
            model->indices.data = (uint32_t*)cursor;
            // Extract the geometry indices. The num_face_num_verts are the faces defined in
            // the OBJ file - lines starting with 'f' char, for example:
            //  f 47/1/1 3/2/1 45/3/1
            //     ^     ^      ^  I'm extracting these indices.
            // Those lines specify indices for vertices positions, UVs and normals.
            // num_face_num_verts count of lines starting with 'f' - number of faces, described
            // by the indices.
            for (uint32_t i = 0; i < obj_attr.num_face_num_verts; i++)
            {
               // Get an index for each of the vertices making a face.
               for (int32_t j = 0; j < obj_attr.face_num_verts[i]; j++)
               {
                  const uint32_t v_idx = obj_attr.faces[i * obj_attr.face_num_verts[i] + j].v_idx;
                  const uint32_t vn_idx = obj_attr.faces[i * obj_attr.face_num_verts[i] + j].vn_idx;
                  const uint32_t vt_idx = obj_attr.faces[i * obj_attr.face_num_verts[i] + j].vt_idx;
                  ((uint32_t*)cursor)[indices_count++] = v_idx;
                  // The indices in OBJ file are starting from 1, not 0.
                  // fprintf(stderr, "v %d, vt %d, vn %d\n", v_idx, vt_idx, vn_idx);
               }
            }
            model->indices.len = indices_count;
            cursor += sizeof(uint32_t) * indices_count;

            model->flags |= MESH_FL_INDICES;
         }

         if (flags & MESH_FL_NORMALS) {
            // Save normals.
            fprintf(stderr, "%s:obj_attr.num_normals %d\n", __FUNCTION__, obj_attr.num_normals);
            memcpy(cursor, obj_attr.normals, sizeof(float) * obj_attr.num_normals);
            model->normals = obj_attr.normals;
            model->normals_count = obj_attr.num_normals;
            cursor += sizeof(float) * obj_attr.num_normals;

            model->flags |= MESH_FL_NORMALS;
         }

         // Move data from transient storage into a persistent one.
         // REMEMBER TO UPDATE ALL FIELDS OF THE model STRUCTURE!!!

         model = (struct Mesh*)append_to_persistent(&mem, (void*)model, sizeof(*model), false);
         model->name = (char*)append_to_persistent(&mem, (void*)model->name, strlen(model->name), true);
         cursor = (uint8_t*)get_persistent_tail();

         if (model->flags & MESH_FL_DATA_INTERLEAVED) {
         } else {
            if (model->flags & MESH_FL_VERTICES) {
               memcpy(cursor, model->vertices.data, sizeof(float) * 3 * model->vertices.len);
               model->vertices.data = (float*)cursor;
               cursor += (sizeof(float) * 3 * model->vertices.len);
            }
            if (model->flags & MESH_FL_INDICES) {
               memcpy(cursor, model->indices.data, sizeof(uint32_t) * model->indices.len);
               model->indices.data = (uint32_t*)cursor;
               cursor += (sizeof(uint32_t) * model->indices.len);
            }
            if (model->flags & MESH_FL_NORMALS) {
               memcpy(cursor, model->normals, sizeof(float) * 3 * model->normals_count);
               model->normals = (float*)cursor;
               cursor += (sizeof(float) * 3 * model->normals_count);
            }
         }

         // TODO(michalc): fetch, colors and uvs.

         // Create an asset.

         Asset asset = {};
         // TODO(michalc): this doesn't help much. The idea is that we need to store the Model3D model
         // in a memory. Maybe it's best to partition the persistent memory. Lets say the first megabyte
         // holds the table of the resources. There I could hold things like Model3D. What's here
         // right now is useless. Asset describes the memory chunk in a very general sense.
         // TODO(michalc): at this point it's not really a OBJ asset. It's a mesh.
         asset.type = ASSET_MODEL3D_OBJ;
         asset.size = (intptr_t)cursor - (intptr_t)mem.persistent_tail;
         asset.memory = (void*)model;
         table.assets[table.assetsCount] = asset;
         table.assetsCount += 1;

         fprintf(stderr, "%s:saved asset - size %d, memory %p\n", __FUNCTION__,  asset.size, asset.memory);

         mem.persistent_tail = (void*)cursor;
         break;
      }
      default:
         break;
   }

   return 0;
}

// TODO(michalc): temporary returning a static object
Asset
get_asset(const char* name, uint8_t name_len)
{
   for (uint8_t i = 0; i < ASSETS_MAX_COUNT; i++)
   {
      Asset as = table.assets[i];

      if (as.type == ASSET_MODEL3D_OBJ)
      {
         const Mesh* const mesh = (const Mesh*)as.memory;
         if (memcmp(mesh->name, name, name_len) == 0)
         {
            as = table.assets[i];
            return as;
         }
      }
   }

   // Fallback if it fails to find an asset.
   return table.assets[0];
}

bool mesh_has_normals(struct Mesh* mesh)
{
   if (mesh->flags & MESH_FL_NORMALS) {
      if (mesh->normals && mesh->normals_count > 0) {
         return true;
      }
   }
   return false;
}

}  // namespace storage
