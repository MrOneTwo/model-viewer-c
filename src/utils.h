#ifndef UTILS_H
#define UTILS_H

// TODO(michalc): those variations of PRINT_MEMORY only difer in printf format.
// This can be improved for sure.
#define PRINT_MEMORY_W_X(ptr, m) \
  for (int ___j = 0; ___j < m; ___j++) { \
    printf("(%04d) %p: %04x\n", ___j, ptr + ___j, ptr[___j]); \
  };

#define PRINT_MEMORY_HW_X(ptr, m) \
  for (int ___j = 0; ___j < m; ___j++) { \
    printf("(%04d) %p: %02x\n", ___j, ptr + ___j, ptr[___j]); \
  };

#define PRINT_MEMORY_B_X(ptr, m) \
  for (int ___j = 0; ___j < m; ___j++) { \
    printf("(%04d) %p: %01x\n", ___j, ptr + ___j, ptr[___j]); \
  };

#define PRINT_MEMORY_W_F(ptr, m) \
  for (int ___j = 0; ___j < m; ___j++) { \
    printf("(%04d) %p: %04.03f\n", ___j, ptr + ___j, ptr[___j]); \
  };

#define PRINT_MEMORY_MAT4(mat) \
  for (int ___i = 0; ___i < 4; ___i++) { \
    printf("(%01d) %p: %04.03f %04.03f %04.03f %04.03f\n", ___i, &mat[___i][0], mat[___i][0], mat[___i][1], mat[___i][2], mat[___i][3]); \
  };

#define PRINT_MEMORY_VEC3(v) \
  printf("%p: %04.03f %04.03f %04.03f\n", v, v[0], v[1], v[2]); \

#define PRINT_MEMORY_VEC4(v) \
  printf("%p: %04.03f %04.03f %04.03f %04.03f\n", v, v[0], v[1], v[2], v[3]); \

#endif  // UTILS_H
