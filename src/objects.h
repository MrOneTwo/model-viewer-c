#ifndef OBJECTS_H
#define OBJECTS_H

#include "cglm/cglm.h"

#include "storage.h"
#include "render.h"

namespace object {

struct Camera {
  // Unit vectors in world's space, position (world's space) and scale.
  mat4 transform;
  mat4 transform_target;
  float yaw, roll, pitch;
  float fov;
};

struct Light {
  // Unit vectors in world's space, position (world's space) and scale.
  mat4 transform;
  mat4 transform_target;
};

struct Object {
  mat4 transform;
};

struct Model3D {
  mat4 transform;
  // The data of the mesh.
  storage::Mesh *mesh;
  // The rendering backend information.
  render::Visible visible;
};

// Move camera in it's own frame. If you want to move the camera
// in the world's space then write into it's transform matrix.
void cam_move(struct Camera *c, vec3 delta);

// Convenience function for cam_move.
void cam_move_x(struct Camera *c, float d);

// Convenience function for cam_move.
void cam_move_y(struct Camera *c, float d);

// Convenience function for cam_move.
void cam_move_z(struct Camera *c, float d);

void apply_transform_to_mesh(struct Model3D *model);

} // namespace object

#endif // OBJECTS_H
