#include <stdio.h>
#include <SDL.h>
#include <epoxy/gl.h>
#include <epoxy/glx.h>

#include <x86intrin.h>

#ifdef _WIN32
  #include <windows.h>
#elif __linux__
  #include <unistd.h>
#elif __APPLE__
  #include <mach-o/dyld.h>
#endif

#include "io.h"
#include "base.h"
#include "utils.h"

#include "storage.h"
#include "objects.h"
#include "render.h"
#include "shaders.h"

#include "cglm/cglm.h"


// ---- TYPES

typedef struct window_params_t {
  int x, y;
  int width, height;
} window_params_t;

typedef struct controls_t
{
  float mouse_sensitivity;
} controls_t;


// ---- GLOBALS

float triangle_verts[9] = {0.0f, 0.5f, 0.0f, 0.5f, -0.5f, 0.0f, -0.5f, -0.5f, 0.0f};
int triangle_indices[3] = {0, 1, 2};

float coord_gizmo[6][6] = {{0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f}, {1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f},
                           {0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f}, {0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f},
                           {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f}};

SDL_Window *window;
window_params_t window_params;
controls_t controls;


// ---- FUNCTIONS

static double
get_scale(void)
{
  float dpi;
  SDL_GetDisplayDPI(0, NULL, &dpi, NULL);
#if _WIN32
  return dpi / 96.0;
#else
  return 1.0;
#endif
}


static void
get_exe_filename(char *buf, int sz)
{
#if _WIN32
  int len = GetModuleFileName(NULL, buf, sz - 1);
  buf[len] = '\0';
#elif __linux__
  char path[512];
  sprintf(path, "/proc/%d/exe", getpid());
  int len = readlink(path, buf, sz - 1);
  buf[len] = '\0';
#elif __APPLE__
  unsigned size = sz;
  _NSGetExecutablePath(buf, &size);
#else
  strcpy(buf, "./lite");
#endif
}


static void
init_window_icon(void)
{
#ifndef _WIN32
  #include "../icon.inl"
  (void) icon_rgba_len; /* unused */
  SDL_Surface *surf = SDL_CreateRGBSurfaceFrom(
    icon_rgba, 64, 64,
    32, 64 * 4,
    0x000000ff,
    0x0000ff00,
    0x00ff0000,
    0xff000000);
  SDL_SetWindowIcon(window, surf);
  SDL_FreeSurface(surf);
#endif
}


int
main(int argc, char **argv)
{
   storage::init_storage_memory();
   controls.mouse_sensitivity = 0.01f;

   storage::start_loading(storage::ASSET_MODEL3D_OBJ);

   storage::load_asset("monkey.obj", storage::MESH_FL_VERTICES | storage::MESH_FL_INDICES | storage::MESH_FL_NORMALS);
   storage::load_asset("cube_tris.obj", storage::MESH_FL_VERTICES | storage::MESH_FL_INDICES | storage::MESH_FL_NORMALS);
   storage::end_loading();

   storage::Asset as = {};

   as = storage::get_asset("cube_tris.obj", strlen("cube_tris.obj"));
   storage::Mesh mesh_cube = *((storage::Mesh*)as.memory);
   as = storage::get_asset("monkey.obj", strlen("monkey.obj"));
   storage::Mesh mesh_monkey = *((storage::Mesh*)as.memory);

   struct object::Model3D mod3d_monkey = {.transform = GLM_MAT4_IDENTITY_INIT, .mesh = &mesh_monkey};

   SDL_Log("Loaded: %s", mesh_monkey.name);


#ifdef _WIN32
   HINSTANCE lib = LoadLibrary("user32.dll");
   int (*SetProcessDPIAware)() = (void*) GetProcAddress(lib, "SetProcessDPIAware");
   SetProcessDPIAware();
#endif

   SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);
   SDL_EnableScreenSaver();
   SDL_EventState(SDL_DROPFILE, SDL_ENABLE);
   atexit(SDL_Quit);

#ifdef SDL_HINT_VIDEO_X11_NET_WM_BYPASS_COMPOSITOR /* Available since 2.0.8 */
   SDL_SetHint(SDL_HINT_VIDEO_X11_NET_WM_BYPASS_COMPOSITOR, "0");
#endif
#if SDL_VERSION_ATLEAST(2, 0, 5)
   SDL_SetHint(SDL_HINT_MOUSE_FOCUS_CLICKTHROUGH, "1");
#endif

   SDL_DisplayMode dm;
   SDL_GetCurrentDisplayMode(0, &dm);
   window_params.width = dm.w;
   window_params.height = dm.h;

   window = SDL_CreateWindow(
    "", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, dm.w * 0.8, dm.h * 0.8,
    SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_OPENGL);

   SDL_GLContext context = SDL_GL_CreateContext(window);
   SDL_Log("OpenGL ver %d, GLSL ver %d", epoxy_gl_version(), epoxy_glsl_version());
   SDL_GetWindowSize(window, &window_params.width, &window_params.height);
   glViewport(0, 0, window_params.width, window_params.height);
   glEnable(GL_DEPTH_TEST);
   glDepthFunc(GL_LESS);

   init_window_icon();

   glClearColor(0.4, 0.48, 0.52, 1);

   shaders::load_shaders();
   GLuint shader_prog_geo = shaders::get_shader_by_features(
      (enum shaders::ShaderFeatures)
      (shaders::SHADER_FEATURE_MVP | shaders::SHADER_FEATURE_NORMALS | shaders::SHADER_FEATURE_POSITIONS | shaders::SHADER_FEATURE_UNIFORM_COLOR)
   ).gl_id;
   GLuint shader_prog_gizmo = shaders::get_shader_by_features(
      (enum shaders::ShaderFeatures)
      (shaders::SHADER_FEATURE_MVP | shaders::SHADER_FEATURE_POSITIONS | shaders::SHADER_FEATURE_VERT_COLOR)
   ).gl_id;
   GLuint shader_prog_light = shaders::get_shader_by_features(
      (enum shaders::ShaderFeatures)
      (shaders::SHADER_FEATURE_MVP | shaders::SHADER_FEATURE_POSITIONS | shaders::SHADER_FEATURE_UNIFORM_COLOR)
   ).gl_id;

   mat4 model = GLM_MAT4_IDENTITY_INIT;
   mat4 view = GLM_MAT4_IDENTITY_INIT;
   mat4 projection = GLM_MAT4_IDENTITY_INIT;

   // Here is where per object transforms come into play. The transforms hold position, scale and
   // orientation of the objects.
   struct object::Object o_light = {};
   glm_mat4_identity(o_light.transform);

   float mesh_cube_transformed[3 * mesh_cube.vertices.len] = {};

   // Transform mesh and gizmo - this bakes the transform into that mesh which means that transforming
   // it in the future will accumulate transforms - NOT GOOD for meshes changing in time.
   {
      vec3 scale = {100.0f, 100.0f, 100.0f};
      glm_scale(mod3d_monkey.transform, scale);

      apply_transform_to_mesh(&mod3d_monkey);
   }
   {
      vec3 offset = {0.0f, 200.0f, 0.0f};
      glm_translate(o_light.transform, offset);

      vec3 scale = {40.0f, 40.0f, 40.0f};
      glm_scale(o_light.transform, scale);

      for (uint32_t i = 0; i < mesh_cube.vertices.len; i++)
      {
         vec3 v = {mesh_cube.vertices.data[3 * i],
                   mesh_cube.vertices.data[3 * i + 1],
                   mesh_cube.vertices.data[3 * i + 2]};

         // Tranform and write back into a new buffer, to avoid loosing the actual vertices.
         glm_mat4_mulv3(o_light.transform, v, 1.0f, mesh_cube_transformed + 3 * i);
      }
   }

   // Setup for drawing the coordinate system gizmo.
   render::Visible vis_gizmo = {};

   render::vao_begin_definition(&vis_gizmo, shader_prog_gizmo);
   render::vao_add_vbo_pos(&vis_gizmo, coord_gizmo[0], sizeof(float) * 6);

   glUseProgram(vis_gizmo.shader_prog);
   {
      glBindBuffer(GL_ARRAY_BUFFER, vis_gizmo.vbo);
      GLint pos_attrib = glGetAttribLocation(vis_gizmo.shader_prog, "a_position");
      if (pos_attrib == -1) {
         SDL_Log("Failed to get attrib: %s", "a_position");
      } else {
         glVertexAttribPointer(pos_attrib, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float), 0);
         glEnableVertexAttribArray(pos_attrib);
      }

      GLint col_attrib = glGetAttribLocation(vis_gizmo.shader_prog, "a_color");
      if (col_attrib == -1) {
         SDL_Log("Failed to get attrib: %s", "a_color");
      } else {
         glVertexAttribPointer(col_attrib, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float), (void*)(3*sizeof(float)));
         glEnableVertexAttribArray(col_attrib);
      }
   }

   // Setup for drawing the light cube.
   render::Visible vis_light = {};
   render::vao_begin_definition(&vis_light, shader_prog_light);
   render::vao_add_vbo_pos(&vis_light, mesh_cube_transformed, mesh_cube.vertices.len);
   render::vao_add_ebo(&vis_light, mesh_cube.indices);

   glUseProgram(vis_light.shader_prog);
   {
      GLint color_loc = glGetUniformLocation(vis_light.shader_prog, "color");
      if (color_loc == -1) {
         SDL_Log("Failed to get : %s", "color");
      } else {
         glUniform3f(color_loc, 1.0f, 0.0f, 0.0f);
      }
   }

   render::Visible vis_monkey = {};
   render::vao_begin_definition(&vis_monkey, shader_prog_geo);
   render::vao_add_vbo_pos(&vis_monkey, mod3d_monkey.mesh->vertices.data, mod3d_monkey.mesh->vertices.len);
   render::vao_add_ebo(&vis_monkey, mod3d_monkey.mesh->indices);
   render::vao_add_normals(&vis_monkey, mod3d_monkey.mesh->normals, mod3d_monkey.mesh->normals_count);

   mod3d_monkey.visible = vis_monkey;

   glUseProgram(mod3d_monkey.visible.shader_prog);
   {
      GLint color_loc = glGetUniformLocation(mod3d_monkey.visible.shader_prog, "color");
      if (color_loc == -1) {
         SDL_Log("Failed to get : %s", "color");
      } else {
         glUniform3f(color_loc, 0.5f, 0.5f, 0.5f);
      }
   }

   SDL_GL_SwapWindow(window);

   struct object::Camera cam = {};
   cam.fov = M_PI/6.0f;
   glm_mat4_identity(cam.transform);
   vec3 offset = {0.0f, 0.0f, -1200.0f};
   glm_translate(cam.transform, offset);

   // GL_FILL
   glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

   uint64_t perf_count_frequency = SDL_GetPerformanceFrequency();
   uint64_t last_counter = SDL_GetPerformanceCounter();
   uint64_t last_cycle_count = _rdtsc();

   SDL_Event event;
   bool running = true;

   while(running)
   {
      uint64_t end_counter = SDL_GetPerformanceCounter();
      uint64_t counter_elapsed = end_counter - last_counter;
      double ms_per_frame = (((1000.0f * (double)counter_elapsed) / (double)perf_count_frequency));
      double fps = (double)perf_count_frequency / (double)counter_elapsed;
      last_counter = end_counter;

      uint64_t end_cycle_count = _rdtsc();
      uint64_t cycles_elapsed = end_cycle_count - last_cycle_count;
      double mcpf = (double)cycles_elapsed;
      last_cycle_count = end_cycle_count;


      // COLLECT INPUT

      int delta_mouse_x, delta_mouse_y;
      uint32_t m_state = SDL_GetRelativeMouseState(&delta_mouse_x, &delta_mouse_y);
      if (SDL_BUTTON(SDL_BUTTON_LEFT) & m_state)
      {
         cam.yaw += controls.mouse_sensitivity * static_cast<float>(delta_mouse_x);
         cam.pitch += controls.mouse_sensitivity * static_cast<float>(delta_mouse_y);
         if (cam.pitch < (-M_PI/2.0f) * 0.8f) cam.pitch = (-M_PI/2.0f) * 0.8f;
         if (cam.pitch > (M_PI/2.0f) * 0.8f) cam.pitch = (M_PI/2.0f) * 0.8f;
      }

      // TODO(michalc): there is no normalization in the movement here.
      const uint8_t *state = SDL_GetKeyboardState(NULL);
      if (state[SDL_SCANCODE_S])
      {
         cam_move_z(&cam, -10.0f);
      }
      if (state[SDL_SCANCODE_W])
      {
         cam_move_z(&cam, 10.0f);
      }
      if (state[SDL_SCANCODE_D])
      {
         cam_move_x(&cam, -10.0f);
      }
      if (state[SDL_SCANCODE_A])
      {
         cam_move_x(&cam, 10.0f);
      }
      if (state[SDL_SCANCODE_E])
      {
         cam_move_y(&cam, -10.0f);
      }
      if (state[SDL_SCANCODE_Q])
      {
         cam_move_y(&cam, 10.0f);
      }

      while(SDL_PollEvent(&event))
      {
         running = event.type != SDL_QUIT;

         if (event.type == SDL_WINDOWEVENT)
         {
            switch (event.window.event) {
               case SDL_WINDOWEVENT_RESIZED:
               case SDL_WINDOWEVENT_SIZE_CHANGED:
               case SDL_WINDOWEVENT_MINIMIZED:
               case SDL_WINDOWEVENT_MAXIMIZED:
                  SDL_GetWindowSize(window, &window_params.width, &window_params.height);
                  glViewport(0, 0, window_params.width, window_params.height);
                  break;
               default: break;
            }
         }
         else if (event.type == SDL_KEYDOWN)
         {
            switch (event.key.keysym.sym) {
               case SDLK_ESCAPE:
                  running = false;
                  break;
               case SDLK_h:
                  break;
               case SDLK_l:
                  break;
               default: break;
            }
         }
         else if (event.type == SDL_MOUSEBUTTONDOWN)
         {
            switch (event.button.button)
            {
               case SDL_BUTTON_LEFT:
                  break;
               default: break;
            }
         }
         else if (event.type == SDL_MOUSEBUTTONUP)
         {
            switch (event.button.button)
            {
               case SDL_BUTTON_LEFT:
               break;
               default: break;
            }
         }
      }

      // PREPARE FOR RENDERING

      // Compute new M, V, P.
      {
         {
            // If you want to transform the model matrix, this is where you should do it.
            glm_mat4_identity(model);
         }

         // Compute the view matrix which transforms the points in such a way as if their new positions
         // were from the camera's perspective.
         {
            // glm_mat4_identity(cam.transform);
            vec4 world_pos_in_cam_ref = GLM_VEC4_BLACK_INIT;

            glm_rotate(cam.transform, cam.yaw, render::V3_UP);

            // Both: world's unit vectors in camera's reference frame and camera's unit vectors in
            // world's reference frame are encoded in the cam.transform. You just need to know if it's
            // in the column or in the row. I think cglm is storing stuff in columns first so what I'm
            // actually dooing below is reading a row... I think.
            vec4 cam_x_in_world_frame;
            cam_x_in_world_frame[0] = cam.transform[0][0];
            cam_x_in_world_frame[1] = cam.transform[1][0];
            cam_x_in_world_frame[2] = cam.transform[2][0];
            cam_x_in_world_frame[3] = cam.transform[3][0];
            glm_rotate(cam.transform, cam.pitch, cam_x_in_world_frame);

            cam.pitch = 0.0f;
            cam.yaw = 0.0f;

            // The camera's position vector in world's frame needs to be converted to world's position vector
            // in the camera's frame. The transformations above can be used for that. They represent the
            // difference between the world's frame and camera's frame.
            mat4 r;
            vec4 t, s;
            glm_decompose(cam.transform, t, r, s);
            glm_mat4_mulv(r, cam.transform[3], world_pos_in_cam_ref);

            glm_mat4_copy(cam.transform, view);
            glm_vec4_copy(world_pos_in_cam_ref, view[3]);
         }

         {
            // glm_mat4_identity(projection);
            // glm_ortho(
            //   CUSTOM_3D_SPACE_MIN_X * (float)window_params.width/window_params.height, CUSTOM_3D_SPACE_MAX_X * (float)window_params.width/window_params.height,
            //   CUSTOM_3D_SPACE_MIN_Y, CUSTOM_3D_SPACE_MAX_Y,
            //   CUSTOM_3D_SPACE_MIN_Z, CUSTOM_3D_SPACE_MAX_Z,
            //   projection);

            glm_perspective(cam.fov,
                           (float)window_params.width/window_params.height,
                           1.0f,
                           CUSTOM_3D_SPACE_MAX_Z,
                           projection);
         }
      }


      // RENDER
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      glClearColor(0.4, 0.48, 0.52, 1);

      render::shader_set_mvp(mod3d_monkey.visible.shader_prog, model, view, projection);
      glBindVertexArray(mod3d_monkey.visible.vao);
      glDrawElements(GL_TRIANGLES, mod3d_monkey.mesh->indices.len, GL_UNSIGNED_INT, 0);

      glLineWidth(3.0);
      // TODO(michal): temporary scaling for the gizmo.
      vec3 gizmo_scale = {40.0f, 40.0f, 40.0f};
      mat4 gizmo_model = GLM_MAT4_IDENTITY_INIT;
      glm_scale(gizmo_model, gizmo_scale);
      render::shader_set_mvp(vis_gizmo.shader_prog, gizmo_model, view, projection);
      glBindVertexArray(vis_gizmo.vao);
      glDrawArrays(GL_LINES, 0, 6);
      glLineWidth(1.0);

      // Animate the light rotating around the center
      {
         glm_rotate(o_light.transform, SDL_GetTicks() / 100000.0f, render::V3_FRONT);

         for (uint32_t i = 0; i < mesh_cube.vertices.len; i++)
         {
            vec3 v = {mesh_cube.vertices.data[3 * i],
                     mesh_cube.vertices.data[3 * i + 1],
                     mesh_cube.vertices.data[3 * i + 2]};

            glm_mat4_mulv3(o_light.transform, v, 1.0f, mesh_cube_transformed + 3 * i);
         }
      }
      glBindBuffer(GL_ARRAY_BUFFER, vis_light.vbo);
      glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * 3 * mesh_cube.vertices.len, mesh_cube_transformed);

      glBindVertexArray(vis_light.vao);
      render::shader_set_mvp(vis_light.shader_prog, model, view, projection);
      glDrawElements(GL_TRIANGLES, mesh_cube.indices.len, GL_UNSIGNED_INT, 0);

      SDL_GL_SwapWindow(window);

      SDL_Delay(16);
   }

   SDL_GL_DeleteContext(context);
   SDL_DestroyWindow(window);

   return EXIT_SUCCESS;
}
