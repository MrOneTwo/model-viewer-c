#include "render.h"
#include "shaders.h"

#include <SDL.h>


namespace render
{
// cglm has compound literals for this but not all compilers support those...
vec3 V3_UP = {0.0f, 1.0f, 0.0f};
vec3 V3_FRONT = {0.0f, 0.0f, 1.0f};

int
vao_begin_definition(Visible* vis, GLuint shader_prog)
{
  glGenVertexArrays(1, &vis->vao);
  glBindVertexArray(vis->vao);

  vis->shader_prog = shader_prog;

  return 0;
}

int
vao_add_vbo_pos(Visible* vis, const void* const vertices, const int vert_count)
{
  glGenBuffers(1, &vis->vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vis->vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * vert_count, vertices, GL_DYNAMIC_DRAW);

  // Necessary for glGetAttribLocation.
  glUseProgram(vis->shader_prog);

  GLint attr_pos = glGetAttribLocation(vis->shader_prog, "a_position");
  if (attr_pos == -1)
  {
    SDL_Log("Failed to get attrib: %s", "a_position");
  } else {
    glVertexAttribPointer(attr_pos, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float), 0);
    glEnableVertexAttribArray(attr_pos);
  }

  return 0;
}

int vao_add_ebo(Visible* vis, struct bufu32 indices)
{
  glGenBuffers(1, &vis->ebo);
  if (glGetError()) {
    SDL_LogError(0, "Couldn't glGenBuffers");
    return -1;
  }
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vis->ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t) * indices.len, indices.data, GL_STATIC_DRAW);

  return 0;
}

int
vao_add_normals(Visible* vis, const void* const normals, const int normals_count)
{
   glGenBuffers(1, &vis->normbo);
   if (glGetError()) {
      SDL_LogError(0, "Couldn't glGenBuffers");
      return -1;
   }
   glBindBuffer(GL_ARRAY_BUFFER, vis->normbo);
   glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * normals_count, normals, GL_DYNAMIC_DRAW);

   // Necessary for glGetAttribLocation.
   glUseProgram(vis->shader_prog);

   const char* const what = "a_normal";
   GLint attr_norm = glGetAttribLocation(vis->shader_prog, what);
   if (attr_norm == -1) {
      SDL_Log("Failed to get attrib: %s, for shader %d", what, vis->shader_prog);
   } else {
      glVertexAttribPointer(attr_norm, 3, GL_FLOAT, GL_FALSE, 0, 0);
      glEnableVertexAttribArray(attr_norm);
   }

   return 0;
}

int
vao_add_vbo_pos_normals(Visible* vis, const void* const data, const int el_count)
{
   glGenBuffers(1, &vis->vbo);
   glBindBuffer(GL_ARRAY_BUFFER, vis->vbo);
   glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * el_count, data, GL_DYNAMIC_DRAW);

   // Necessary for glGetAttribLocation.
   glUseProgram(vis->shader_prog);

   GLint attr_pos = glGetAttribLocation(vis->shader_prog, "a_position");
   GLint attr_norm = glGetAttribLocation(vis->shader_prog, "a_normal");
   if (attr_pos == -1 || attr_norm == -1)
   {
      SDL_Log("Failed to get attrib: %s %d, %s %d", "a_position", attr_pos, "a_normal", attr_norm);
   } else {
      glVertexAttribPointer(attr_pos, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float), 0);
      // TODO(michalc): should it be?
      const bool normalized = false;
      // glVertexAttribPointer uses the last argument differently, depending if VBO was bound.
      // If it was bound, then it uses is just as a number - a byte offset.
      glVertexAttribPointer(attr_norm, 3, GL_FLOAT, normalized ? GL_TRUE : GL_FALSE, 6*sizeof(float), (void*)(3*sizeof(float)));
      glEnableVertexAttribArray(attr_pos);
      glEnableVertexAttribArray(attr_norm);
   }

   return 0;
}

int
init_from_flags(Visible* vis, storage::Mesh* mesh, GLuint shader_prog, enum VisibleFlags flags)
{
   render::vao_begin_definition(vis, shader_prog);
   render::vao_add_vbo_pos(vis, mesh->vertices.data, mesh->vertices.len);
   render::vao_add_ebo(vis, mesh->indices);
   if (storage::mesh_has_normals(mesh)) {
      render::vao_add_normals(vis, mesh->normals, mesh->normals_count);
   }

   return 0;
}

// int vao_add_colors(Visible* vis,
//                    const void* const colors, const int colors_count)
// {
//   glGenBuffers(1, &vis->normbo);
//   if (glGetError()) {
//     SDL_LogError(0, "Couldn't glGenBuffers");
//     return -1;
//   }
//   glBindBuffer(GL_ARRAY_BUFFER, vis->normbo);
//   glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * normals_count, normals, GL_DYNAMIC_DRAW);
// }


// int vao_add_colors(Visible* vis, const char* const attrib_name,
//                      const void* const colors, const int colors_count)
// {
//   glGenBuffers(1, &vis->normbo);
//   if (glGetError()) {
//     SDL_LogError(0, "Couldn't glGenBuffers");
//     return -1;
//   }
//   glBindBuffer(GL_ARRAY_BUFFER, vis->normbo);
//   glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * normals_count, normals, GL_DYNAMIC_DRAW);

//   // Necessary for glGetAttribLocation.
//   glUseProgram(vis->shader_prog);

//   GLint attr_norm = glGetAttribLocation(vis->shader_prog, "a_normal");
//   if (attr_norm == -1) {
//     SDL_Log("Failed to get attrib: %s, for shader %d", "a_normal");
//   } else {
//     glVertexAttribPointer(attr_norm, 3, GL_FLOAT, GL_FALSE, 0, 0);
//     glEnableVertexAttribArray(attr_norm);
//   }

//   return 0;
// }

void
shader_set_mvp(GLuint shader_prog, mat4 model, mat4 view, mat4 projection)
{
  glUseProgram(shader_prog);

  GLint model_loc = shaders::SHADER_LAYOUT_VS_UNI_MODEL;
  if (model_loc == -1)
  {
    SDL_Log("Failed to get : %s", "model");
  }
  glUniformMatrix4fv(model_loc, 1, GL_FALSE, (float *)model);

  GLint view_loc = shaders::SHADER_LAYOUT_VS_UNI_VIEW;
  if (view_loc == -1)
  {
    SDL_Log("Failed to get : %s", "view");
  }
  glUniformMatrix4fv(view_loc, 1, GL_FALSE, (float *)view);

  GLint projection_loc = shaders::SHADER_LAYOUT_VS_UNI_PROJECTION;
  if (projection_loc == -1)
  {
    SDL_Log("Failed to get : %s", "projection");
  }
  glUniformMatrix4fv(projection_loc, 1, GL_FALSE, (float *)projection);
}

// int
// shader_set_vec3(GLuint shader_prog, vec3 data, char* const name)
// {
//   GLint attrib = glGetAttribLocation(shader_prog, name);
//   if (attrib == -1)
//   {
//     SDL_Log("Failed to get attrib: %s", name);
//     return -1;
//   }
//   glVertexAttribPointer(attrib, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float), 0);
//   glEnableVertexAttribArray(attrib);

//   return 0;
// }

GLuint shader_create(const GLchar *vs_source, const GLchar *fs_source)
{
   int compilation_status = 0;
   char gl_log[512];
   GLuint vertex = glCreateShader(GL_VERTEX_SHADER);
   GLuint fragment = glCreateShader(GL_FRAGMENT_SHADER);

   glShaderSource(vertex, 1, &vs_source, NULL);
   glShaderSource(fragment, 1, &fs_source, NULL);

   glCompileShader(vertex);
   glGetShaderiv(vertex, GL_COMPILE_STATUS, &compilation_status);
   if (!compilation_status) {
      glGetShaderInfoLog(vertex, 512, NULL, gl_log);
      SDL_Log("Vertex shader compilation failed: %s", gl_log);
   }

   glCompileShader(fragment);
   glGetShaderiv(fragment, GL_COMPILE_STATUS, &compilation_status);
   if (!compilation_status) {
      glGetShaderInfoLog(fragment, 512, NULL, gl_log);
      SDL_Log("Fragment shader compilation failed: %s", gl_log);
      SDL_Log("%s", fs_source);
   }

   GLuint shader_prog = glCreateProgram();
   glAttachShader(shader_prog, vertex);
   glAttachShader(shader_prog, fragment);
   glLinkProgram(shader_prog);

   glGetProgramiv(shader_prog, GL_LINK_STATUS, &compilation_status);
   if (!compilation_status) {
      glGetProgramInfoLog(shader_prog, 512, NULL, gl_log);
      SDL_Log("Shader linking failed: %s", gl_log);
   }

   return shader_prog;
}

} // namespace render
