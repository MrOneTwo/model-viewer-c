#ifndef STORAGE_H_
#define STORAGE_H_

#include "base.h"

#include <stdlib.h>

#define ASSETS_MAX_COUNT 128

namespace storage
{
typedef struct Memory {
  bool32 isInitialized;

  uint64 transientMemorySize;
  void* transientMemory;
  void* transient_tail;

  uint64 persistentMemorySize;
  void* persistentMemory;
  void* persistent_tail;
} Memory;

typedef enum AssetType {
  ASSET_NONE,
  ASSET_MODEL3D_OBJ,
  ASSET_MODEL3D_GLTF,
  ASSET_FONT_TTF,
  ASSET_TYPE_COUNT
} AssetType;

typedef struct Asset {
  AssetType type;
  uint32 size;
  void* memory;
} Asset;

typedef struct AssetTable {
  uint32 assetsCount;
  Asset assets[ASSETS_MAX_COUNT];
} AssetTable;

enum MeshFlags {
   MESH_FL_VERTICES = 0b1 << 0,
   MESH_FL_INDICES = 0b1 << 1,
   MESH_FL_NORMALS = 0b1 << 2,
   MESH_FL_DATA_INTERLEAVED = 0b1 << 3,
};

typedef struct Mesh {
   int flags;
   char* name;
   struct buff vertices;
   struct bufu32 indices;
   float* normals;
   uint32_t normals_count;
   float* verticesColors;
   float* uvs;
} Mesh;

bool mesh_has_normals(struct Mesh* mesh);

extern Memory mem;

void* get_transient_tail(void);
void* get_persistent_tail(void);
void* append_to_persistent(struct Memory* mem, void* data, size_t bytes_count, bool zero_terminate);

void init_storage_memory(void);

void start_loading(AssetType type);
int8_t load_asset(const char* path, int flags);
void end_loading();

Asset get_asset(const char* name, uint8_t name_len);
}

#endif  // STORAGE_H_
