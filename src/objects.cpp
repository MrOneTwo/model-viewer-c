#include "objects.h"
#include "utils.h"

namespace object {

void
cam_move(struct Camera *c, vec4 delta) {
  // Convert vector in cam's frame to a vector in world's frame.
  mat4 r;
  vec4 t, s;
  glm_decompose(c->transform, t, r, s);
  glm_mat4_inv(r, r);
  glm_mat4_mulv(r, delta, delta);

  glm_vec4_add(t, delta, delta);
  glm_vec4_copy(delta, c->transform[3]);
}

void
cam_move_x(struct Camera *c, float d) {
  vec4 delta = {};
  delta[0] = d;
  cam_move(c, delta);
}

void
cam_move_y(struct Camera *c, float d) {
  vec4 delta = {};
  delta[1] = d;
  cam_move(c, delta);
}

void
cam_move_z(struct Camera *c, float d) {
  vec4 delta = {};
  delta[2] = d;
  cam_move(c, delta);
}

void
apply_transform_to_mesh(struct Model3D *model) {
  for (uint32_t i = 0; i < model->mesh->vertices.len; i++) {
    vec3 v = {model->mesh->vertices.data[3 * i],
              model->mesh->vertices.data[3 * i + 1],
              model->mesh->vertices.data[3 * i + 2]};

    // Transform...
    glm_mat4_mulv3(model->transform, v, 1.0f, v);

    // ... and write back.
    model->mesh->vertices.data[3 * i] = v[0];
    model->mesh->vertices.data[3 * i + 1] = v[1];
    model->mesh->vertices.data[3 * i + 2] = v[2];
  }
}

} // namespace object
