#ifndef SHADERS_H
#define SHADERS_H

namespace shaders
{
   enum ShaderFeatures {
      SHADER_FEATURE_MVP = 0b1 << 0,
      SHADER_FEATURE_POSITIONS = 0b1 << 1,
      SHADER_FEATURE_NORMALS = 0b1 << 2,
      SHADER_FEATURE_VERT_COLOR = 0b1 << 3,
      SHADER_FEATURE_UNIFORM_COLOR = 0b1 << 4,
   };

   struct Shader {
      char label[32];
      unsigned int gl_id;
      enum ShaderFeatures features;
   };

   enum shader_layout_vs {
      SHADER_LAYOUT_VS_UNI_MODEL = 0,
      SHADER_LAYOUT_VS_UNI_VIEW = 1,
      SHADER_LAYOUT_VS_UNI_PROJECTION = 2,
   };

/* Load shaders from their source files and try to compile them.
 */
int load_shaders(void);

int get_shader_source_by_name(const char * looking_for, const char **out, char v_or_f);

struct Shader get_shader_geo();
struct Shader get_shader_light();
struct Shader get_shader_by_features(enum ShaderFeatures features);

/*
 * VERTEX SHADERS
 */

extern const char* shd_vs_default;
extern const char* shd_vs_transformations;
extern const char* shd_vs_light;
extern const char* shd_vs_texture;
extern const char* shd_vs_planes;


/*
 * FRAGMENT SHADERS
 */

extern const char* shd_fs_default;
extern const char* shd_fs_texture;

} // namespace

#endif  // SHADERS_H
