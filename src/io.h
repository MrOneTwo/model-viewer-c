/**
 * \file io.h
 *
 * These are convenience functions, that use SDL's I/O API.
 */

#ifndef IO_H
#define IO_H

#include "base.h"

namespace io
{

enum err_io {
  NONE = 0,
  INVALID_ARG = -1,
  INVALID_FILE_PATH = -2,
};

enum err_io erred(void);

int list_dir_contents(const char *const dir_path, char* out);

uint64_t load_entire_file(const char* fileName, void* buf, char type);

/**
 * \brief Function used by the Tiny OBJ loader.
 *
 */
void obj_loader_file_reader(void* ctx, const char* filename, int is_mtl, const char* obj_filename, char** buf, long unsigned int* len);

} // namespace

#endif  // IO_H
