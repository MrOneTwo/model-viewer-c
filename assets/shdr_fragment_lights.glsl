#version 460
// TODO(mc): why does this create problems?
// precision mediump float;
// varying is same as in for fragment shader
in vec3 v_color;

out vec4 frag_color;
void main()
{
  frag_color = vec4(v_color.xyz, 1.0);
}
