#version 460
// TODO(mc): why does this create problems?
// precision mediump float;
// varying is same as in for fragment shader
in vec3 v_color;
in vec3 v_normal;
in vec2 v_texCoords;
in vec3 frag_pos;

//#define NUM_LIGHTS 1
//uniform vec3 lights[NUM_LIGHTS];

vec3 light_pos = vec3(0, 300, 100);

out vec4 frag_color;

void main()
{
   vec3 norm = normalize(v_normal);
   vec3 light_dir = normalize(light_pos - frag_pos);
   float diff = max(dot(norm, light_dir), 0.0);
   // vec3 diffuse = diff * lightColor;
   //

  //for (int i = 0; i < NUM_LIGHTS; i++)
  //{
  //}

  frag_color = vec4(v_color.xyz * diff, 1.0);
}
