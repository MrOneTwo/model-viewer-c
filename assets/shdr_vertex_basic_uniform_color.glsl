#version 460

// uniforms meaning values same for every vert
layout(location = 0) uniform mat4 model;
layout(location = 1) uniform mat4 view;
layout(location = 2) uniform mat4 projection;

// attribute is same as in for vertex shader
layout(location = 3) in vec3 a_position;

// varying is same as out for vertex shader
out vec3 v_color;
uniform vec3 color;

void main()
{
   gl_Position = projection * view * model * vec4(a_position.xyz, 1.0);
   v_color = color;
}
