#version 120

// attribute is same as in for vertex shader
attribute vec3 a_position;
attribute vec3 a_color;
// varying is same as out for vertex shader
varying vec3 v_color;
// uniforms meaning values same for every vert

void main()
{
  gl_Position = vec4(a_position.xyz, 1.0);
  v_color = a_color;
}
