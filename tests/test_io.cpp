#include "io.h"

#include <stdio.h>
#include <cstdlib>
#include <cassert>
#include <stdint.h>
#include <cstring>

#define TEST_SUCCESS 0
#define TEST_FAILURE -1

static uint8_t* buf;

int
test_load_entire_file(void)
{
   int ret = io::load_entire_file("tests/shdr_vertex_basic.glsl", buf);

   assert(ret == 304);
   // Check last 2 characters.
   assert(strncmp("}\n", (char*)(buf + 304 - 2), 2) == 0);

   if (io::erred()) {
      return  TEST_FAILURE;
   }

   return TEST_SUCCESS;
}

int
test_list_dir_contents(void)
{
   (void)io::list_dir_contents(".", (char*)buf);

   if (io::erred()) {
      return TEST_FAILURE;
   }

   return TEST_SUCCESS;
}

int
main(int argc, char **argv)
{
   int ret = EXIT_SUCCESS;

   buf = (uint8_t*)malloc(1024);

   assert(test_load_entire_file() == 0);
   assert(test_list_dir_contents() == 0);

   exit(ret);

   return 0;
}
